import java.util.*;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /** Actual main method to run examples and everything. */
   public void run() {
      Graph g = new Graph ("G");
      g.createRandomSimpleGraph (12, 2);
      System.out.println("==== GRAPH: ====");
      System.out.println (g);

      System.out.println("==== SOLUTION: ====");
      findPrerequisiteSteps(g);
   }
   public void findPrerequisiteSteps(Graph g){
      Vertex v = g.first;
      HashMap<String, List<String>> courseMap1 = new HashMap<>(); // course, prerequisite course list
      HashMap<String, Integer> courseMap3 = new HashMap<>(); // course, term number
      HashMap<Integer, List<String>> courseMap4 = new HashMap<>(); // term number, course list
      while (v != null) {
         // System.out.println(v.id); // DEBUG
         if (v.first != null) { // if there is arc
            Arc a = v.first;
            while (a != null) {
               String targetV_id = a.target.id;
               if (!courseMap1.containsKey(v.id)) {
                  courseMap1.put(v.id, new ArrayList<>());
               }
               if (!courseMap1.containsKey(targetV_id)) { // if arc target (järeldusaine) is not already in map
                  courseMap1.put(targetV_id, new ArrayList<>()); // then put it there
               }
               courseMap1.get(targetV_id).add(v.id);
               if(a.next != null) {
                  a = a.next;
               } else {
                  a = null;
               }
            }
         } else {
            if (!courseMap1.containsKey(v.id)) {
               courseMap1.put(v.id, new ArrayList<>());
            }
         }
         if(v.next != null) {
            v = v.next;
         } else {
            v = null;
         }
      }
      // System.out.println(courseMap1.toString()); // DEBUG
      for (Map.Entry<String, List<String>> entry : courseMap1.entrySet()) {
         int lvl = 1;
         String parentCourseName = entry.getKey();
         List<String> prereqList = entry.getValue();
         if (!courseMap3.containsKey(parentCourseName)) {
            courseMap3.put(parentCourseName, lvl);
         } else {
            lvl = courseMap3.get(parentCourseName);
         }

         while (prereqList.size() > 0){
            lvl++;
            for (String prereqCourseName : prereqList) {
               //System.out.println(parentCourseName+" lvl is "+lvl); // DEBUG
               courseMap3.put(parentCourseName, lvl);
               prereqList = courseMap1.get(prereqCourseName);
            }
         }
         courseMap3.put(parentCourseName, lvl);
      }
      // System.out.println(courseMap3.toString()); // DEBUG
      System.out.println("In what semester you need to take what course: "+courseMap3.toString());
      for (Map.Entry<String,Integer> entry : courseMap3.entrySet()) {
         Integer semesterNr = entry.getValue();
         String courseName = entry.getKey();
         if (!courseMap4.containsKey(semesterNr)) {
            List<String> newCourseList = new ArrayList<>();
            newCourseList.add(courseName);
            courseMap4.put(semesterNr, newCourseList);
         } else {
            courseMap4.get(semesterNr).add(courseName);
         }
      }
      System.out.println("What courses in what semester you need to take: "+courseMap4.toString());
   }

   /** ASSIGNMENT
    * Find the quickest way to finish school by what course to take in which semester in regards to prerequisite courses.
    */
   class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;
      // You can add more fields, if needed

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }
   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;
      // You can add more fields, if needed

      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }
   } 


   class Graph {

      private String id;
      private Vertex first;
      private int info = 0;
      // You can add more fields, if needed

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer();
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].id + "_"
                  + varray [i].id, varray [vnr], varray [i]);
               /* disable undirected arcs
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
                */
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         /*
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException 
               ("Impossible number of edges: " + m);
          */
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            System.out.println("connected: " + connected [j].length);
            if (i==j) 
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0 || connected [j].length != 0) // added last if statement to disable cycles
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.id + "_" + vj.id, vi, vj);
            connected [i][j] = 1;
            /*  disable undirected arcs
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
             */
            edgeCount--;  // a new edge happily created
         }
      }
   }

} 

